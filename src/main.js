// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import firebase from 'firebase'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './styles/app.scss'

Vue.config.productionTip = false

var config = {
  apiKey: 'AIzaSyBx9G_dBaXBen_U401FmX5L2-XNQItvDtI',
  authDomain: 'invoice-fffa6.firebaseapp.com',
  databaseURL: 'https://invoice-fffa6.firebaseio.com',
  projectId: 'invoice-fffa6',
  storageBucket: 'invoice-fffa6.appspot.com',
  messagingSenderId: '42925995895'
}

firebase.initializeApp(config)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
